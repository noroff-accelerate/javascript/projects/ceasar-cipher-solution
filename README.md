# Ceasar Cipher 

Simple HTML page for encrypting and decrypting with a ceasar cipher

## Usage

Open the `index.html` with your browser.

## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

## License

Unlicensed © 2019 Noroff Accelerate AS