
// Craig Marais, 2020

let input, output, alphabet, shift

function loadValues() {
	input = document.getElementById('formInput').value
	output = document.getElementById('formOutput').value
	alphabet = document.getElementById('formAlphabet').value
	if(alphabet === ""){
		console.log("using default alphabet")
		alphabet = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
	}
	alphabet = [...alphabet]
	shift = document.getElementById('formShift').value
	if(shift  === ""){
		shift = 3
		document.getElementById('formShift').value = 3
	}

	console.log("INPUT:    " + input)
	console.log("OUTPUT:   " + output)
	console.log("ALPHABET: " + alphabet)
	console.log("SHIFT:    " + shift)
}

function getShiftedChar(char, shift){
	if(char === " "){
		return char
	}
	shift = parseInt(shift,10)

	let old = alphabet.indexOf(char)
	let index = alphabet.indexOf(char)

	index = index + shift

	while(index < 0){
		index += alphabet.length

	}
	while(index >= alphabet.length){
		index -= alphabet.length
	}

	return alphabet[index]
}

function countWords(sentence){
	sentence = sentence
				.toLowerCase()
				.replace('.', '')
				.replace(',', '')
				.replace('!', '')
				.replace('?', '')
				.replace(';', '')
				.replace(':', '')
				.replace('-', ' ')
	let words = sentence.split(' ')

	let count = 0

	for (key in words){
		if(englishWords.includes(words[key]) || englishWords.includes(words[key].slice(0, -1)))
		{
			count += 1
		}
	}
	return count
}

function encrypt() {
	console.log("\nencrypting ...")
	loadValues()
	let solution = ""
	chars = [...input]
	for(char in chars){
		solution += getShiftedChar(chars[char], shift)
	}
	document.getElementById('formOutput').value = solution

}

function decrypt() {
	console.log("\ndecrypting ...")
	loadValues()

	let solutions = []

	let bestSolution = ""
	let bestShift = 0
	let bestCount = 0

	for (let i = 0; i < alphabet.length; i++) {
		let solution = ""
		chars = [...input]
		for(char in chars){
			solution += getShiftedChar(chars[char], i)
		}
		let count = countWords(solution)
		if(count)
		{
			solutions.push({solution, count})
			console.log(solution,count)

			if(count > bestCount){
				bestSolution = solution
				bestShift = i
				bestCount = count
			}
			else if(count == bestCount){
				let upperNew = solution.length - solution.replace(/[A-Z]/g, '').length; 
				let upperOld = bestSolution.length - bestSolution.replace(/[A-Z]/g, '').length;
				console.log(bestSolution + " (" + upperOld + ") vs. (" + upperNew + ") " + solution)
				if(upperNew < upperOld)
					{
					bestSolution = solution
					bestShift = i
					bestCount = count
				}
			}
		}
	}
	if(bestShift > alphabet.length /2){
		bestShift -= alphabet.length
	}
	document.getElementById('formOutput').value = bestSolution
	document.getElementById('formShift').value = bestShift
}
